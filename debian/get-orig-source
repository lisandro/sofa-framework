#!/bin/sh
# get source for sofa-framework which assembles sofa-framework, sofa-modules and sofa-applications

set -e
# set -x
NAME=`dpkg-parsechangelog | awk '/^Source/ { print $2 }'`

if ! echo $@ | grep -q upstream-version ; then
    VERSION=`dpkg-parsechangelog | awk '/^Version:/ { print $2 }' | sed 's/\([0-9\.]\+\)-[0-9]\+$/\1/'`
    # regard dversionmangle
    VERSION=`echo ${VERSION} | sed 's/\([0-9]\)~\([br]\)/\1-\2/'`
else
    VERSION=`echo $@ | sed 's?^.*--upstream-version \([-0-9.rcbeta]\+\) .*sofa-framework.*?\1?'`
    if echo "$VERSION" | grep -q "upstream-version" ; then
        echo "Unable to parse version number"
        exit
    fi
fi


TARBALLDIR=../tarballs
mkdir -p ${TARBALLDIR}

indexpage=`grep www.sofa-framework.org debian/watch | sed 's/^.*\(http:[^[:space:]]\+\)[[:space:]].*$/\1/'`
cd ${TARBALLDIR}
wget -q -N "$indexpage"
if [ -e ../${NAME}-${VERSION}.zip ] ; then
    mv ../${NAME}-${VERSION}.zip .
else
    wget -q -N --no-check-certificate `grep framework.*\.zip download | sed "s?^.*\(https://gforge.inria.fr/frs/download.php/[0-9]\+/sofa-framework-${VERSION}\.zip\).*?\1?"`
fi
wget -q -N --no-check-certificate `grep modules.*\.zip download | sed "s?^.*\(https://gforge.inria.fr/frs/download.php/[0-9]\+/sofa-modules-${VERSION}\.zip\).*?\1?"`
wget -q -N --no-check-certificate `grep applications.*\.zip download | sed "s?^.*\(https://gforge.inria.fr/frs/download.php/[0-9]\+/sofa-applications-${VERSION}\.zip\).*?\1?"`
rm download

OUTPUTDIR=sofa-${VERSION}
rm -rf ${OUTPUTDIR}
mkdir -p ${OUTPUTDIR}
cd ${OUTPUTDIR}

find .. -mindepth 1 -maxdepth 1 -name "sofa-*.zip" -exec unzip -q \{\} \;
find .. -mindepth 1 -maxdepth 1 -name "sofa-*.zip" -delete

rm -rf extlibs/miniBoost       # libboost-dev
rm -rf extlibs/libQGLViewer*   # libqglviewer-dev
rm -rf extlibs/eigen*          # libeigen3-dev
# rm -rf extlibs/qwt*            # libqwt5-qt4-dev
rm -rf extlibs/newmat          # libnewmat10-dev
rm -rf extlibs/csparse         # libsuitesparse-dev
rm -rf extlibs/tinyxml         # libtinyxml-dev (>= 2.6)
# the external libraries are still there:
# LML  miniFlowVR  PML tinyxml

cd ..

# might need to mangle version (untested)
VERSION=`echo ${VERSION} | sed 's/\([0-9]\)-\([br]\)/\1~\2/'`
GZIP="--best --no-name" tar czf sofa-framework_${VERSION}.orig.tar.gz ${OUTPUTDIR}
rm -rf ${OUTPUTDIR}
